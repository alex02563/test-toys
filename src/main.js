import Vue from 'vue'
import App from './App.vue'
import router from './route/index'
import vuetify from './plugins/vuetify'
import './plugins/loading'
import './plugins/leaflet'
import './plugins/axios'
import './assets/scss/main.scss'

Vue.config.productionTip = false

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
