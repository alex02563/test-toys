import Vue from 'vue'
import VueLoading from 'vue-loading-overlay'
import 'vue-loading-overlay/dist/vue-loading.css'

Vue.use(VueLoading, {
  canCancel: false,
  color: '#e91e63',
  loader: 'dots',
  width: 100,
  height: 100,
  backgroundColor: '#ff9fc0',
  isFullPage: true,
  opacity: 0.7
})
